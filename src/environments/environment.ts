// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyALOG1iODGl1-xpZfY8LhWJK6x6Yq49WO0',
    authDomain: 'musicapp-3cea1.firebaseapp.com',
    databaseURL: 'https://musicapp-3cea1.firebaseio.com',
    projectId: 'musicapp-3cea1',
    storageBucket: 'musicapp-3cea1.appspot.com',
    messagingSenderId: '22560115402'
  }
};
