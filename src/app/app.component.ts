import { Component } from '@angular/core';
import {FirebaseService} from "./firebase.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[FirebaseService]
})
export class AppComponent {
  title = 'app works!';
  constructor(
    private firebaseService: FirebaseService,
  ){

  }


  getToken() {
    this.firebaseService.getToken();
  }
}
