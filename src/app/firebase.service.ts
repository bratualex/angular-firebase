import {Inject, Injectable} from '@angular/core';
import {FirebaseApp} from 'angularfire2';
import * as firebase from 'firebase';
@Injectable()
export class FirebaseService {

  private _messaging: firebase.messaging.Messaging;
  private _token: string;
  private _registration: any;
  private _notificationMessage;

  constructor(@Inject(FirebaseApp) private _firebaseApp: any) {
    this._messaging = firebase.messaging(this._firebaseApp);
    this._messaging.requestPermission()
      .then(() => {
        console.log('Notification permission granted.');
      })
      .catch((error) => {
        console.log(error);
        console.log('Notification permission NOT granted.');
      });

    this._messaging.onMessage(function (payload) {
      console.log('Message received. ', payload);
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       // How can I access this._notificationMessage here   ??????
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    });
  }

  getToken() {
    const that = this;
    this._messaging.getToken()
      .then((currentToken) => {
        if (currentToken) {
          that._token = currentToken;
          console.log('Request token available');
        } else {
          // Show permission request.
          console.log('No Instance ID token available. Request permission to generate one.');
        }
      })
      .catch( (err) => {
        console.log('An error occurred while retrieving token. ', err);
      });

    /*this._messaging.onTokenRefresh(function () {
      this._messaging.getToken()
        .then(function (refreshedToken) {
          console.log('Token refreshed.');
        })
        .catch(function (err) {
          console.log('Unable to retrieve refreshed token ', err);
        });
    });*/
  }


}
